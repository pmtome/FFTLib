function FFTLib_Test_Envelope()
    clc; close all;

    fs = 200e6;
    load('private/Test_Envelope_Signal.mat');
    uLSB = sqrt(6 * fs) * 1e-6;
    xr = floor(x / uLSB + 0.5) * uLSB;
    
    fprintf('The signal under test is the complex envelope of a 40 MHz\n');
    fprintf('OFDM signal with 2048 256-QAM subcarriers. It has a peak\n');
    fprintf('power of 1 W (for a 50 Ohm load) and a peak-to-average\n');
    fprintf('power ratio (PAPR) of 10.7 dB. The signal has been\n');
    fprintf('quantized so that there is a noise floor of 1 uV/rHz\n');
    fprintf('(-137 dBW/Hz for a 50 Ohm load).\n');
    
    figure();
    fres = 33e3;
    FFTLib.PlotSpectrum(xr, fs, fres, 'MHz', 'nW/Hz');
    m = FFTLib.GetSpectrum(xr, fs, fres, 'MHz', 'W/Hz');
    grid on; title(sprintf('Mean Signal Power = %g dBW\nSpectrum Integral = %g dBW\nNMSE = %g dB', 10*log10(var(xr)/50/2), 10*log10(sum(m)*fres), 10*log10(nmse(var(xr)/50/2,sum(m)*fres))));
    
    figure();
    FFTLib.PlotSpectrum(xr, fs, 33e3, 'MHz', 'logV/rHz');
    grid on; title('Floor = 7.07e-7 V/rHz');
    
    figure();
    FFTLib.PlotSpectrum(xr, fs, 33e3, 'MHz', 'loguV/rHz');
    grid on; title('Floor = 0.707 uV/rHz');
    
    figure();
    FFTLib.PlotSpectrum(xr, fs, 33e3, 'MHz', 'dBW/Hz');
    grid on; title('Floor = -140 dBW/Hz');
    
    figure();
    FFTLib.PlotSpectrum(xr, fs, 33e3, 'MHz', 'dBm/Hz');
    grid on; title('Floor = -110 dBm/Hz');
    
    figure();
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW/Hz', 'NoWindow'); hold on;
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW/Hz', 'blackmanharris', 'periodic');
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW/Hz', 'hanning', 'periodic');
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW/Hz', 'blackman', 'periodic');
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW/Hz', 'hft116d', 'periodic');    
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW/Hz', 'hft144d', 'periodic');
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW/Hz', 'hft248d', 'periodic');
    grid on;
    title('Multiple Windows (noiseless signal)');
    legend('Rectangular', 'Blackman-Harris', 'Hanning', 'Blackman', 'HFT116D', 'HFT144D', 'HFT248D');
    
    figure();
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW', 'NoWindow'); hold on;
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW', 'blackmanharris', 'periodic');
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW', 'hanning', 'periodic');
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW', 'blackman', 'periodic');
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW', 'hft116d', 'periodic');    
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW', 'hft144d', 'periodic');
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW', 'hft248d', 'periodic');
    grid on;
    title('Multiple Windows (noiseless signal)');
    legend('Rectangular', 'Blackman-Harris', 'Hanning', 'Blackman', 'HFT116D', 'HFT144D', 'HFT248D');
    
    
    for i = get(gcf,'Number') : -1 : 1
        figure(i);
    end
end