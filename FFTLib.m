% FFTLib v2.1
% Pedro Tome', 2017
%
% FFTLib.PlotSpectrum(signal, samplingFrequency, frequencyResolution)
% FFTLib.PlotSpectrum(__, frequencyUnits)
% FFTLib.PlotSpectrum(__, magnitudeUnits)
% FFTLib.PlotSpectrum(__, loadImpedance)
% FFTLib.PlotSpectrum(__, frequencyRange)
% FFTLib.PlotSpectrum(__, signalType)
% FFTLib.PlotSpectrum(__, windowFunction, windowArguments)
% outStruct = FFTLib.PlotSpectrum(__)
%
% [magnitude, frequency] = FFTLib.GetSpectrum(signal, samplingFrequency, frequencyResolution)
% [__] = FFTLib.GetSpectrum(__, frequencyUnits)
% [__] = FFTLib.GetSpectrum(__, magnitudeUnits)
% [__] = FFTLib.GetSpectrum(__, loadImpedance)
% [__] = FFTLib.GetSpectrum(__, frequencyRange)
% [__] = FFTLib.GetSpectrum(__, signalType)
% [__] = FFTLib.GetSpectrum(__, windowFunction, windowArguments)
% [__, outStruct] = FFTLib.GetSpectrum(__)
%
%
% Valid Units:
% Base Units:  'Hz', 'W', 'W/Hz', 'V', 'V/rHz'
% SI Prefixes: 'Y', 'Z', 'E', 'P', 'T', 'G', 'M', 'k',
%              'm', 'u', 'n', 'p', 'f', 'a', 'z', 'y'
% Modifiers:   'dB', 'log'
% Other:       'dBm', 'dBm/Hz', 'normalized'
%
% Examples: 'MHz', 'dBW/Hz', 'mV', 'uV/rHz', 'loguV/rHz'
%
%
% Valid Options:
% Frequency Range: 'centered', 'onesided', 'twosided'
% Signal Type:     'envelope', 'passband'
% Window Function: 'NoWindow', 'hft144d', 'blackmanharris', 'hanning', etc.
% Load Impedance:  <numeric scalar> (in Ohm)


classdef FFTLib

    properties (Constant)
        VERSION = '2.2';
        frequencyRanges  = {'onesided', 'twosided', 'centered'};
        signalTypes      = {'passband', 'envelope'};
        windows          = {'NoWindow'};  % Or windows like 'hanning' or 'blackmanharris'
    end

    methods (Static)

        function [magnitude, frequency, outStruct] = GetSpectrum(varargin)
            %% Set up default options
            defaults.frequencyUnits  = struct('modifier','', 'scaler',1, 'baseUnit','Hz', 'legendStr','Hz');
            defaults.magnitudeUnits  = struct('modifier','dB', 'scaler',1, 'baseUnit','W/Hz', 'legendStr','dBW/Hz');
            defaults.frequencyRange  = 'centered';
            defaults.windowFunction  = 'hft144d';
            defaults.windowArguments = {'periodic'};
            defaults.load            = 50;


            %% Parse input arguments
            args = parseInputArguments(varargin, defaults);


            %% Compute spectrum
            [magnitude, frequency, ENBW, NENBW] = simple_pwelch(        ...
                args.signal, args.samplingFrequency,                    ...
                args.frequencyResolution, args.frequencyRange,          ...
                args.windowFunction, args.windowArguments);


            %% Scale magnitude and frequency
            magnitude = scaleMagnitude(                                 ...
                magnitude, args.magnitudeUnits, args.load, ENBW,        ...
                args.signalType);
            frequency = scaleFrequency(frequency, args.frequencyUnits);
            
            
            %% Set up output arguments
            if (nargout > 0)
                outStruct.FrequencyResolution = args.frequencyResolution;
                outStruct.ENBW = ENBW;
                outStruct.NENBW = NENBW;
            end
        end


        function [outStruct] = PlotSpectrum(varargin)
            %% Set up default options
            defaults.frequencyUnits  = struct('modifier','', 'scaler',1, 'baseUnit','Hz', 'legendStr','Hz');
            defaults.magnitudeUnits  = struct('modifier','dB', 'scaler',1, 'baseUnit','W/Hz', 'legendStr','dBW/Hz');
            defaults.frequencyRange  = 'centered';
            defaults.windowFunction  = 'hft144d';
            defaults.windowArguments = {'periodic'};
            defaults.load            = 50;


            %% Parse input arguments
            % Extract first optional axes handle and remove it from varargin if present
            if (~isempty(varargin) && length(varargin{1}) == 1 && isgraphics(varargin{1}))
                ax = varargin{1};
                varargin = varargin(2:end);
            else
                ax = gca;
            end

            args = parseInputArguments(varargin, defaults);
            args.axes = ax;


            %% Compute spectrum
            [magnitude, frequency, ENBW, NENBW] = simple_pwelch(        ...
                args.signal, args.samplingFrequency,                    ...
                args.frequencyResolution, args.frequencyRange,          ...
                args.windowFunction, args.windowArguments);


            %% Scale magnitude and frequency
            magnitude = scaleMagnitude(                                 ...
                magnitude, args.magnitudeUnits, args.load, ENBW,        ...
                args.signalType);
            frequency = scaleFrequency(frequency, args.frequencyUnits);


            %% Plot spectrum
            hPlot = plot(args.axes, frequency, magnitude);
            xlabel(strcat('Frequency (', args.frequencyUnits.legendStr, ')'));
            if (strcmpi(args.magnitudeUnits.modifier, 'normalized'))
                ylabel(strcat('Normalized Power Spectral Density (',args.magnitudeUnits.legendStr,')'));
            elseif (strcmpi(args.magnitudeUnits.baseUnit, 'W/Hz'))
                ylabel(strcat('Power Spectral Density (',args.magnitudeUnits.legendStr,')'));
            elseif (strcmpi(args.magnitudeUnits.baseUnit, 'V/rHz'))
                ylabel(strcat('Amplitude Spectral Density (',args.magnitudeUnits.legendStr,')'));
            elseif (strcmpi(args.magnitudeUnits.baseUnit, 'W'))
                ylabel(strcat('Power Spectrum (',args.magnitudeUnits.legendStr,')'));
            elseif (strcmpi(args.magnitudeUnits.baseUnit, 'V'))
                ylabel(strcat('Amplitude Spectrum (',args.magnitudeUnits.legendStr,')'));
            end

            if (strcmp(args.frequencyUnits.modifier, 'log'))
                set(get(hPlot,'Parent'), 'XScale', 'log');
            end
            if (strcmp(args.magnitudeUnits.modifier, 'log'))
                set(get(hPlot,'Parent'), 'YScale', 'log');
            end


            %% Set up output arguments
            if (nargout > 0)
                outStruct.PlotHandle = hPlot;
                outStruct.FrequencyResolution = args.frequencyResolution;
                outStruct.ENBW = ENBW;
                outStruct.NENBW = NENBW;
            end
        end

    end
end