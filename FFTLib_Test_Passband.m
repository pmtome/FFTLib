function FFTLib_Test_Passband()
    clc; close all;

    fs = 200e6;
    t = (0 : 100e3-1) / fs;
    x = sqrt(2*50)*sin(2*pi*10e6*t) + sqrt(2*50*0.5)*sin(2*pi*20.333e6*t);
    uLSB = sqrt(6 * fs) * 1e-6;
    xr = floor(x / uLSB + 0.5) * uLSB;
    
    fprintf('The signal under test is the sum of a 10 MHz tone of\n');
    fprintf('7.07 Vrms (1 W for a 50 Ohm load) and a 20.333 MHz\n');
    fprintf('tone of 5 Vrms (0.5 W for a 50 Ohm load).\n');
    fprintf('The signal has been quantized so that there is a\n');
    fprintf('noise floor of 1 uV/rHz (-137 dBW/Hz for a 50 Ohm load).\n');
    
    
    figure();
    subplot(1,2,1);
    FFTLib.PlotSpectrum(xr, fs, 100e3, 'MHz', 'V');
    grid on; title('Peaks = [3.54, 5.00, 5.00, 3.54] V');
    subplot(1,2,2);
    FFTLib.PlotSpectrum(xr, fs, 100e3, 'MHz', 'V', 'onesided');
    grid on; title('Peaks = [7.07, 5.00] V');
    
    figure();
    subplot(1,2,1);
    FFTLib.PlotSpectrum(xr, fs, 100e3, 'MHz', 'logV/rHz');
    grid on; title('Floor = 7.07e-7 V/rHz');
    subplot(1,2,2);
    FFTLib.PlotSpectrum(xr, fs, 100e3, 'MHz', 'logV/rHz', 'onesided');
    grid on; title('Floor = 1e-6 V/rHz');
    
    figure();
    subplot(1,2,1);
    FFTLib.PlotSpectrum(xr, fs, 100e3, 'MHz', 'loguV/rHz');
    grid on; title('Floor = 0.707 uV/rHz');
    subplot(1,2,2);
    FFTLib.PlotSpectrum(xr, fs, 100e3, 'MHz', 'loguV/rHz', 'onesided');
    grid on; title('Floor = 1 uV/rHz');
    
    figure();
    subplot(1,2,1);
    FFTLib.PlotSpectrum(xr, fs, 100e3, 'MHz', 'dBW');
    grid on; title('Peaks = [-6, -3, -3, -6] dBW');
    subplot(1,2,2);
    FFTLib.PlotSpectrum(xr, fs, 100e3, 'MHz', 'dBW', 'onesided');
    grid on; title('Peaks = [0, -3] dBW');
    
    figure();
    subplot(1,2,1);
    FFTLib.PlotSpectrum(xr, fs, 100e3, 'MHz', 'dBW/Hz');
    grid on; title('Floor = -140 dBW/Hz');
    subplot(1,2,2);
    FFTLib.PlotSpectrum(xr, fs, 100e3, 'MHz', 'dBW/Hz', 'onesided');
    grid on; title('Floor = -137 dBW/Hz');
    
    figure();
    subplot(1,2,1);
    FFTLib.PlotSpectrum(xr, fs, 100e3, 'MHz', 'dBm/Hz');
    grid on; title('Floor = -110 dBm/Hz');
    subplot(1,2,2);
    FFTLib.PlotSpectrum(xr, fs, 100e3, 'MHz', 'dBm/Hz', 'onesided');
    grid on; title('Floor = -107 dBm/Hz');
    
    figure();
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW', 'NoWindow'); hold on;
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW', 'blackmanharris', 'periodic');
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW', 'hanning', 'periodic');
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW', 'blackman', 'periodic');
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW', 'hft116d', 'periodic');    
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW', 'hft144d', 'periodic');
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW', 'hft248d', 'periodic');
    grid on;
    title('Multiple Windows (noiseless signal)');
    legend('Rectangular', 'Blackman-Harris', 'Hanning', 'Blackman', 'HFT116D', 'HFT144D', 'HFT248D');
    
    figure();
    subplot(1,2,1);
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW', 'NoWindow'); hold on;
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW', 'blackmanharris', 'periodic');
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW', 'hanning', 'periodic');
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW', 'blackman', 'periodic');
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW', 'hft116d', 'periodic');    
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW', 'hft144d', 'periodic');
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW', 'hft248d', 'periodic');
    grid on;
    title({'On-Grid Frequency Component', 'Peak = -3.01 dBW'});
    legend('Rectangular', 'Blackman-Harris', 'Hanning', 'Blackman', 'HFT116D', 'HFT144D', 'HFT248D', 'Location', 'South');
    axis([9.985 10.015 -3.08 -3.00]);
    
    subplot(1,2,2);
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW', 'NoWindow'); hold on;
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW', 'blackmanharris', 'periodic');
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW', 'hanning', 'periodic');
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW', 'blackman', 'periodic');
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW', 'hft116d', 'periodic');    
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW', 'hft144d', 'periodic');
    FFTLib.PlotSpectrum(x, fs, 10e3, 'MHz', 'dBW', 'hft248d', 'periodic');
    grid on;
    title({'Off-Grid Frequency Component', 'Peak = -6.02 dBW'});
    legend('Rectangular', 'Blackman-Harris', 'Hanning', 'Blackman', 'HFT116D', 'HFT144D', 'HFT248D', 'Location', 'South');
    axis([20.315 20.345 -8.00 -5.90]);
    
    for i = get(gcf,'Number') : -1 : 1
        figure(i);
    end
end