% Returns the N-point HFT144D window.
% If SFLAG is specified and equal to 'periodic', then hft144d
% computes a length N+1 window and returns the first N points.
%
% PSLL:  -144.1 dB
% NENBW: 4.5386 bins
% Emax:  +0.0021 dB
% ROV:   79.9%
%
% PSLL: peak sidelobe level, NENBW: normalized equivalent noise bandwidth,
% Emax: maximum amplitude estimation error, ROV: recommended overlap.
%
% Ref: G. Heinzel, A. R�diger, and R. Schilling. (Feb. 2002). Spectrum and
%      spectral density estimation by the Discrete Fourier transform (DFT),
%      including a comprehensive list of window functions and some new
%      flat-top windows. Max Planck Society, [Online]. Available: 
%      http://hdl.handle.net/11858/00-001M-0000-0013-557A-5


function [w] = hft144d(N,SFLAG)
    periodic = ( nargin == 2 && strcmpi(SFLAG, 'periodic') );
    if (periodic)
        N = N + 1;
    end
    
    
    % w = c(1)*cos(0*z) + c(2)*cos(1*z) + c(3)*cos(2*z) + ...
    c = [1 -1.96760033 1.57983607 -0.81123644 0.22583558 -0.02773848 0.00090360].';
    
    z = 2*pi * (0 : N-1)' / N;
    w = cos( z * (0 : length(c)-1) ) * c;
    
    
    if (periodic)
        w = w(1 : end-1);
    end
end