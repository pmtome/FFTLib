function [magOut] = scaleMagnitude(magIn, units, load, ENBW, signalType)
    if (strcmpi(signalType, 'passband'))
        magOut = scale_passband(magIn, units, load, ENBW);
    elseif (strcmpi(signalType, 'envelope'))
        magOut = scale_envelope(magIn, units, load, ENBW);
    end
end


function [magOut] = scale_passband(magIn, units, load, ENBW)
    if (strcmp(units.baseUnit, 'W/Hz'))       % Power Spectral Density
        magOut = magIn / load / units.scaler;
    elseif (strcmp(units.baseUnit, 'V/rHz'))  % Amplitude Spectral Density
        magOut = sqrt(magIn) / units.scaler;
    elseif (strcmp(units.baseUnit, 'W'))      % Power Spectrum
        magOut = magIn / load * ENBW / units.scaler;
    elseif (strcmp(units.baseUnit, 'V'))      % Amplitude Spectrum
        magOut = sqrt(magIn * ENBW) / units.scaler;
    end
    
    if (strcmp(units.modifier, 'dB'))
        magOut = 10 * log10(magOut);
    elseif (strcmp(units.modifier, 'normalized'))
        m = 10*log10( magIn/load/units.scaler );  % dBW/Hz
        magOut = m - max(m);
    end
end


function [magOut] = scale_envelope(magIn, units, load, ENBW)
    if (strcmp(units.baseUnit, 'W/Hz'))       % Power Spectral Density
        magOut = magIn / load / units.scaler / 2;
    elseif (strcmp(units.baseUnit, 'V/rHz'))  % Amplitude Spectral Density
        magOut = sqrt(magIn / 2) / units.scaler;
    elseif (strcmp(units.baseUnit, 'W'))      % Power Spectrum
        magOut = magIn / load * ENBW / units.scaler / 2;
    elseif (strcmp(units.baseUnit, 'V'))      % Amplitude Spectrum
        magOut = sqrt(magIn * ENBW / 2) / units.scaler;
    end
    
    if (strcmp(units.modifier, 'dB'))
        magOut = 10 * log10(magOut);
    elseif (strcmp(units.modifier, 'normalized'))
        m = 10*log10( magIn/load/units.scaler/2 );  % dBW/Hz
        magOut = m - max(m);
    end
end