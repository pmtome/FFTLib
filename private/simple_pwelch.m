function [magnitude, frequency, ENBW, NENBW] = simple_pwelch(signal, samplingFrequency, frequencyResolution, frequencyRange, windowFunction, windowArguments)
    %% Setup Welch's method
    segmentLength = round(samplingFrequency / frequencyResolution);

    if (nargin < 4 || strcmpi(windowFunction, 'NoWindow'))
        window = ones(segmentLength, 1);
    else
        window = feval(windowFunction, segmentLength, windowArguments{:});
    end

    noverlap = round(segmentLength * optimumWindowOverlap(window));
    nfft = segmentLength;


    %% Use Welch's method
    if (strcmpi(frequencyRange, 'centered'))
        [magnitude, frequency] = pwelch(signal, window, noverlap, nfft, samplingFrequency, 'centered', 'psd');
    elseif (strcmpi(frequencyRange, 'onesided'))
        [magnitude, frequency] = pwelch(signal, window, noverlap, nfft, samplingFrequency, 'onesided', 'psd');
    elseif (strcmpi(frequencyRange, 'twosided'))
        [magnitude, frequency] = pwelch(signal, window, noverlap, nfft, samplingFrequency, 'twosided', 'psd');
    end


    %% Calculate ENBW and NENBW
    S1 = sum(window);
    S2 = sum(window.^2);
    ENBW = samplingFrequency * S2 / S1^2;
    NENBW = ENBW / frequencyResolution;
end