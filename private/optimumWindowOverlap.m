function [optOverlap] = optimumWindowOverlap(window, plotFlag, plotOverlap)
    %% Interpolate Window (for speed)
    interpLength = 1000;
    window = interp1( (0:length(window)-1)/length(window), window, (0:interpLength-1)/interpLength )';
    
    
    overlap = (0 : 0.01 : 1)';
    
    
    %% Amplitude Flatness
    AF = zeros(size(overlap));
    numWindows = 20;
    
    if (nargin > 1 && plotFlag)
        figure();
    end
    if (nargin < 3)
        plotOverlap = 0.4;
    end
    
    for i = 1 : length(overlap)
        numSeparationSamples = round(length(window) * (1-overlap(i)));
        summedWindows = zeros(length(window) + (numWindows-1)*numSeparationSamples, 1);
        baseWindow = [window ; zeros((numWindows-1)*numSeparationSamples,1)];
        for j = 1 : numWindows
            newWindow = circshift(baseWindow , (j-1)*numSeparationSamples);
            summedWindows = summedWindows + newWindow;
            
            if (nargin > 1 && plotFlag && abs(overlap(i) - plotOverlap) < eps)
                plot(newWindow); hold on;
            end
        end
        
        crossings = diff( sign(diff(summedWindows)) );
        peaksIndices   = crossings < 0;
        valleysIndices = crossings > 0;
        
        peaks   = summedWindows(peaksIndices);
        valleys = summedWindows(valleysIndices);
        
        peak   = mode(peaks);  % mode(x) = most frequent value of x
        valley = mode(valleys);
        
        % This function is only accurate up to overlap = 1 - 1/numWindows
        % (eg.: numWindows = 20 --> accurate up to overlap = 0.95)
        if (overlap(i) < 1 - 1/numWindows)
            AF(i) = valley / peak;
        else
            AF(i) = 1;
        end
        
        if (nargin > 1 && plotFlag && abs(overlap(i) - plotOverlap) < eps)
            plot(summedWindows, 'k-', 'LineWidth', 1.2);
            plot(find(peaksIndices), peaks, 'kx');
            plot(find(valleysIndices), valleys, 'ko');
            xlim([0 3.4e3]);
        end
    end
    
    
    %% Overlap Correlation
    OC = zeros(size(overlap));
    for i = 1 : length(overlap)
        numOverlapSamples = round(length(window) * overlap(i));
        circWindow = circshift(window, numOverlapSamples);
        OC(i) = sum(window(1:numOverlapSamples-1) .* circWindow(1:numOverlapSamples-1)) ./ sum(window.^2);
    end
    
    
    %% Optimum Overlap
    [~,idx] = max(AF - OC);
    optOverlap = overlap(idx);
    
    
    %% Plot Flag
    if (nargin > 1 && plotFlag)
        figure();
        plot(overlap, AF, overlap, OC);
        hold on;
        
        ymin = ylim; ymin = ymin(1);
        plot([optOverlap optOverlap], [ymin OC(idx)], 'k--');
        plot([optOverlap optOverlap], [OC(idx) AF(idx)], 'k');
        plot([0 optOverlap], [AF(idx) AF(idx)], 'k--');
        xlabel('Overlap'); ylabel('Amplitude Flatness | Overlap Correlation');
        legend('AF', 'OC', 'Location', 'SouthEast');
        title(sprintf('Optimum Overlap: %g', optOverlap));
    end
end