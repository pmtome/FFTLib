function [args] = parseInputArguments(inputArguments, defaults)
    args = defaults;
    
    
    % Compulsory arguments
    args.signal = inputArguments{1};
    args.samplingFrequency = inputArguments{2};
    args.frequencyResolution = inputArguments{3};
    inputArguments = inputArguments(4:end);
    
    
    % Compute frequencyResolution if argument is 'max'
    if (ischar(args.frequencyResolution) && strcmpi(args.frequencyResolution,'max'))
        args.frequencyResolution = args.samplingFrequency / length(args.signal);
    end
    
    
    % Round frequencyResolution to nearest possible value
    nfft = round(args.samplingFrequency / args.frequencyResolution);
    args.frequencyResolution = args.samplingFrequency / nfft;
    
    
    % Autodetect signal type (real passband or complex envelope)
    if (isreal(args.signal))
        args.signalType = 'passband';
    else
        args.signalType = 'envelope';
    end
    
    
    for i = 1 : length(inputArguments)
        % Frequency Units
        if (ischar(inputArguments{i}))
            [modifier,scaler,baseUnit,legendStr] = extractUnits(inputArguments{i});
            if (strcmp(baseUnit, 'Hz'))
                args.frequencyUnits.modifier = modifier;
                args.frequencyUnits.scaler = scaler;
                args.frequencyUnits.baseUnit = baseUnit;
                args.frequencyUnits.legendStr = legendStr;
                continue;
            end
        end
        
        % Magnitude Units
        if (ischar(inputArguments{i}))
            [modifier,scaler,baseUnit,legendStr] = extractUnits(inputArguments{i});
            if (~isempty(baseUnit) && ~strcmp(baseUnit, 'Hz'))
                args.magnitudeUnits.modifier = modifier;
                args.magnitudeUnits.scaler = scaler;
                args.magnitudeUnits.baseUnit = baseUnit;
                args.magnitudeUnits.legendStr = legendStr;
                continue;
            end
        end
        
        % Frequency Range
        if (ischar(inputArguments{i}))
            index = ismember(lower(FFTLib.frequencyRanges), lower(inputArguments{i}));
            if (any(index))
                args.frequencyRange = FFTLib.frequencyRanges{index};
                continue;
            end
        end
        
        % Signal Type (real passband or complex envelope)
        if (ischar(inputArguments{i}))
            index = ismember(lower(FFTLib.signalTypes), lower(inputArguments{i}));
            if (any(index))
                args.signalType = FFTLib.signalTypes{index};
                continue;
            end
        end
        
        % Load (cathes all numeric arguments)
        if (isnumeric(inputArguments{i}))
            args.load = inputArguments{i};
            continue;
        end
        
        % Window Function (must come at the end; catches all remaining
        % input arguments as arguments for the window function)
        if (ischar(inputArguments{i}))
            if (strcmpi(inputArguments{i},FFTLib.windows{1}))  % NoWindow
                args.windowFunction = FFTLib.windows{1};
                args.windowArguments = {};
                break;
            else
                args.windowFunction = inputArguments{i};
                args.windowArgs = {};
                if (i < length(inputArguments))
                    args.windowArguments = inputArguments(i+1 : end);
                end
                break;
            end
        end
    end
end