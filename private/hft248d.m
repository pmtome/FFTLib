% Returns the N-point HFT248D window.
% If SFLAG is specified and equal to 'periodic', then hft248d
% computes a length N+1 window and returns the first N points.
%
% PSLL:  -248.4 dB
% NENBW: 5.6512 bins
% Emax:  +0.0009 dB
% ROV:   84.1%
%
% PSLL: peak sidelobe level, NENBW: normalized equivalent noise bandwidth,
% Emax: maximum amplitude estimation error, ROV: recommended overlap.
%
% Ref: G. Heinzel, A. R�diger, and R. Schilling. (Feb. 2002). Spectrum and
%      spectral density estimation by the Discrete Fourier transform (DFT),
%      including a comprehensive list of window functions and some new
%      flat-top windows. Max Planck Society, [Online]. Available: 
%      http://hdl.handle.net/11858/00-001M-0000-0013-557A-5


function [w] = hft248d(N,SFLAG)
    periodic = ( nargin == 2 && strcmpi(SFLAG, 'periodic') );
    if (periodic)
        N = N + 1;
    end
    
    
    % w = c(1)*cos(0*z) + c(2)*cos(1*z) + c(3)*cos(2*z) + ...
    c = [1 -1.985844164102 1.791176438506 -1.282075284005 0.667777530266 -0.240160796576 0.056656381764 -0.008134974479 0.000624544650 -0.000019808998 0.000000132974].';
    
    z = 2*pi * (0 : N-1)' / N;
    w = cos( z * (0 : length(c)-1) ) * c;
    
    
    if (periodic)
        w = w(1 : end-1);
    end
end