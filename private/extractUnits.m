function [modifier,scaler,baseUnit,legendStr] = extractUnits(str)
    SI_PREFIXES = {'y','z','a','f','p','n','u','m',  ...
                   'k','M','G','T','P','E','Z','Y'};
    SI_SCALERS = 10.^[-24,-21,-18,-15,-12,-9,-6,-3,  ...
                      3,6,9,12,15,18,21,24];
    
    
    modifier  = '';
    scaler    = 1;
    baseUnit  = '';
    legendStr = '';
    
    
    %% Length = 0: empty string
    if (isempty(str))
        return;
    end
    
    
    %% Length = 1: no prefix (W or V)
    if (length(str) == 1)
        if (ismember(str, {'W','V'}))
            baseUnit = str;
            legendStr = baseUnit;
        end
        return;
    end
    
    
    %% Length = 2: Hz or <p>V or <p>W
    if (length(str) == 2)
        if (strcmp(str, 'Hz'))
            baseUnit = 'Hz';
            legendStr = 'Hz';
        elseif (ismember(str(1), SI_PREFIXES) && ismember(str(2), {'W','V'}))
            prefix = str(1);
            scaler = SI_SCALERS(ismember(SI_PREFIXES, prefix));
            baseUnit = str(2);
            if (strcmp(prefix, 'u'))
                legendStr = ['\mu' baseUnit];
            else
                legendStr = [prefix baseUnit];
            end
        end
        return;
    end
    
    
    %% Length = 3: dBW or dBV or dBm or <p>Hz
    if (length(str) == 3)
        if (strcmp(str(1:2), 'dB'))
            if (ismember(str(3), {'W','V'}))
                modifier = 'dB';
                baseUnit = str(3);
                legendStr = ['dB' baseUnit];
            elseif (strcmp(str(3), 'm'))
                modifier = 'dB';
                prefix = 'm';
                scaler = SI_SCALERS(ismember(SI_PREFIXES, prefix));
                baseUnit = 'W';
                legendStr = 'dBm';
            end
        elseif (ismember(str(1), SI_PREFIXES) && strcmp(str(2:end), 'Hz'))
            prefix = str(1);
            scaler = SI_SCALERS(ismember(SI_PREFIXES, prefix));
            baseUnit = 'Hz';
            if (strcmp(prefix, 'u'))
                legendStr = '\muHz';
            else
                legendStr = [prefix 'Hz'];
            end
        end
        return;
    end
    
    
    %% Length = 4: dB<p>W or dB<p>V or W/Hz or logW or logV
    if (length(str) == 4)
        if (strcmp(str(1:2), 'dB') && ismember(str(3), SI_PREFIXES) && ismember(str(4), {'W','V'}))
                modifier = 'dB';
                prefix = str(3);
                scaler = SI_SCALERS(ismember(SI_PREFIXES, prefix));
                baseUnit = str(4);
                if (strcmp(prefix, 'u'))
                    legendStr = ['dB' '\mu' baseUnit];
                else
                    legendStr = ['dB' prefix baseUnit];
                end
                if (strcmp(legendStr, 'dBmW'))
                    legendStr = 'dBm';
                end
        elseif (strcmp(str, 'W/Hz'))
            baseUnit = 'W/Hz';
            legendStr = 'W/Hz';
        elseif (strcmp(str(1:3), 'log') && ismember(str(4), {'W','V'}))
            modifier = 'log';
            baseUnit = str(4);
            legendStr = baseUnit;
        end
        return;
    end
    
    
    %% Length = 5: <p>W/Hz or V/rHz or logHz or log<p>W or log<p>V
    if (length(str) == 5)
        if (ismember(str(1), SI_PREFIXES) && strcmp(str(2:end), 'W/Hz'))
            prefix = str(1);
            scaler = SI_SCALERS(ismember(SI_PREFIXES, prefix));
            baseUnit = 'W/Hz';
            if (strcmp(prefix, 'u'))
                legendStr = '\muW/Hz';
            else
                legendStr = [prefix 'W/Hz'];
            end
        elseif (strcmp(str, 'V/rHz'))
            baseUnit = 'V/rHz';
            legendStr = 'V/\surdHz';
        elseif (strcmp(str(1:3), 'log') && strcmp(str(4:end), 'Hz'))
            modifier = 'log';
            baseUnit = 'Hz';
            legendStr = 'Hz';
        elseif (strcmp(str(1:3), 'log') && ismember(str(4), SI_PREFIXES) && ismember(str(5), {'W','V'}))
            modifier = 'log';
            prefix = str(4);
            scaler = SI_SCALERS(ismember(SI_PREFIXES, prefix));
            baseUnit = str(5);
            if (strcmp(prefix, 'u'))
                legendStr = ['\mu' baseUnit];
            else
                legendStr = [prefix baseUnit];
            end
        end
        return;
    end
    
    
    %% Length = 6: dBW/Hz or dBm/Hz or <p>V/rHz or log<p>Hz
    if (length(str) == 6)
        if (strcmp(str, 'dBW/Hz'))
            modifier = 'dB';
            baseUnit = 'W/Hz';
            legendStr = 'dBW/Hz';
        elseif (strcmp(str, 'dBm/Hz'))
            modifier = 'dB';
            prefix = 'm';
            scaler = SI_SCALERS(ismember(SI_PREFIXES, prefix));
            baseUnit = 'W/Hz';
            legendStr = 'dBm/Hz';
        elseif (ismember(str(1), SI_PREFIXES) && strcmp(str(2:end), 'V/rHz'))
            prefix = str(1);
            scaler = SI_SCALERS(ismember(SI_PREFIXES, prefix));
            baseUnit = 'V/rHz';
            if (strcmp(prefix, 'u'))
                legendStr = '\muV/\surdHz';
            else
                legendStr = [prefix 'V/\surdHz'];
            end
        elseif (strcmp(str(1:3), 'log') && ismember(str(4), SI_PREFIXES) && strcmp(str(5:end), 'Hz'))
            modifier = 'log';
            prefix = str(4);
            scaler = SI_SCALERS(ismember(SI_PREFIXES, prefix));
            baseUnit = 'Hz';
            if (strcmp(prefix, 'u'))
                legendStr = '\muHz';
            else
                legendStr = [prefix 'Hz'];
            end
        end
        return;
    end
    
    
    %% Length = 7: dB<p>W/Hz or dBV/rHz or logW/Hz
    if (length(str) == 7)
        if (strcmp(str(1:2), 'dB') && ismember(str(3), SI_PREFIXES) && strcmp(str(4:end), 'W/Hz'))
            modifier = 'dB';
            prefix = str(3);
            scaler = SI_SCALERS(ismember(SI_PREFIXES, prefix));
            baseUnit = 'W/Hz';
            if (strcmp(prefix, 'u'))
                legendStr = 'dB\muW/Hz';
            else
                legendStr = ['dB' prefix 'W/Hz'];
            end
            if (strcmp(legendStr, 'dBmW/Hz'))
                legendStr = 'dBm/Hz';
            end
        elseif (strcmp(str, 'dBV/\surdHz'))
            modifier = 'dB';
            baseUnit = 'V/rHz';
            legendStr = 'dBV/\surdHz';
        elseif (strcmp(str, 'logW/Hz'))
            modifier = 'log';
            baseUnit = 'W/Hz';
            legendStr = 'W/Hz';
        end
        return;
    end
    
    
    %% Length = 8: dB<p>V/rHz or log<p>W/Hz or logV/rHz
    if (length(str) == 8)
        if (strcmp(str(1:2), 'dB') && ismember(str(3), SI_PREFIXES) && strcmp(str(4:end), 'V/rHz'))
            modifier = 'dB';
            prefix = str(3);
            scaler = SI_SCALERS(ismember(SI_PREFIXES, prefix));
            baseUnit = 'V/rHz';
            if (strcmp(prefix, 'u'))
                legendStr = 'dB\muV/\surdHz';
            else
                legendStr = ['dB' prefix 'V/\surdHz'];
            end
        elseif (strcmp(str(1:3), 'log') && ismember(str(4), SI_PREFIXES) && strcmp(str(5:end), 'W/Hz'))
            modifier = 'log';
            prefix = str(4);
            scaler = SI_SCALERS(ismember(SI_PREFIXES, prefix));
            baseUnit = 'W/Hz';
            if (strcmp(prefix, 'u'))
                legendStr = '\muW/Hz';
            else
                legendStr = [prefix 'W/Hz'];
            end
        elseif (strcmp(str, 'logV/rHz'))
            modifier = 'log';
            baseUnit = 'V/rHz';
            legendStr = 'V/\surdHz';
        end
        return;
    end
    
    
    %% Length = 9: log<p>V/rHz
    if (length(str) == 9)
        if (strcmp(str(1:3), 'log') && ismember(str(4), SI_PREFIXES) && strcmp(str(5:end), 'V/rHz'))
            modifier = 'log';
            prefix = str(4);
            scaler = SI_SCALERS(ismember(SI_PREFIXES, prefix));
            baseUnit = 'V/rHz';
            if (strcmp(prefix, 'u'))
                legendStr = '\muV/\surdHz';
            else
                legendStr = [prefix 'V/\surdHz'];
            end
        end
    end
    
    %% Length = 10: normalized
    if (length(str) == 10)
        if (strcmp(str, 'normalized'))
            modifier = 'normalized';
            baseUnit = 'W/Hz';
            legendStr = 'dB/Hz';
        end
    end
end