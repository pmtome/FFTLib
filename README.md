FFTLib
======

FFTLib is a Matlab library for estimating and plotting the spectral density or
spectrum of measured or synthesized signals.

**Examples:**
```
FFTLib.PlotSpectrum(signal, samplingFrequency, frequencyResolution, 'GHz', 'dBW/Hz');
FFTLib.PlotSpectrum(signal, samplingFrequency, frequencyResolution, 'MHz', 'dBm');
[magnitude, frequency] = FFTLib.GetSpectrum(signal, samplingFrequency, frequencyResolution, 'kHz', 'loguV/rHz', 'onesided');
```




<br><br>




## Contents
* [Syntax](#syntax)
* [Description](#description)
* [Units](#units)
* [Examples](#examples)
* [More About](#more-about)
* [References](#references)




<br><br>




## Syntax
```
FFTLib.PlotSpectrum(signal, samplingFrequency, frequencyResolution)
FFTLib.PlotSpectrum(__, frequencyUnits)
FFTLib.PlotSpectrum(__, magnitudeUnits)
FFTLib.PlotSpectrum(__, loadImpedance)
FFTLib.PlotSpectrum(__, frequencyRange)
FFTLib.PlotSpectrum(__, signalType)
FFTLib.PlotSpectrum(__, windowFunction, windowArguments)
outStruct = FFTLib.PlotSpectrum(__)
```
```
[magnitude, frequency] = FFTLib.GetSpectrum(signal, samplingFrequency, frequencyResolution)
[__] = FFTLib.GetSpectrum(__, frequencyUnits)
[__] = FFTLib.GetSpectrum(__, magnitudeUnits)
[__] = FFTLib.GetSpectrum(__, loadImpedance)
[__] = FFTLib.GetSpectrum(__, frequencyRange)
[__] = FFTLib.GetSpectrum(__, signalType)
[__] = FFTLib.GetSpectrum(__, windowFunction, windowArguments)
[__, outStruct] = FFTLib.GetSpectrum(__)
```




<br><br>




## Description
```
FFTLib.PlotSpectrum(signal, samplingFrequency, frequencyResolution)
[magnitude, frequency] = FFTLib.GetSpectrum(signal, samplingFrequency, frequencyResolution)
```
Plots the power spectral density (PSD) of `signal` estimated using Welch's
method of overlapped segmented averaging of modified periodograms.
`samplingFrequency` determines, in Hz, the sampling frequency of `signal`.
`frequencyResolution` determines, in Hz, the frequency resolution of the PSD
estimation. The plot has frequency units of Hz and magnitude units of dBW/Hz
(assuming a load impedance of 50 ohm). The estimation uses the HFT144D flat-top
window (with an 80% overlap), and it is displayed as a double-sided centered
plot. If `signal` has an imaginary part, then it is assumed to be the complex
envelope of a real passband signal. In this case, the PSD estimate is scaled so
as to represent the real passband signal; see [More About](#more-about) for more
information. FFTLib.GetSpectrum returns the magnitude and frequency data of the
PSD estimate instead of plotting it.

----

```
FFTLib.PlotSpectrum(__, frequencyUnits)
[__] = FFTLib.GetSpectrum(__, frequencyUnits)
```
Sets the units of the frequency axis of the plot or the returned `frequency`
vector. See [Units](#units) for a list of valid units. Note that this option is
entirely unrelated to the `samplingFrequency` and `frequencyResolution` input
arguments.

----

```
FFTLib.PlotSpectrum(__, magnitudeUnits)
[__] = FFTLib.GetSpectrum(__, magnitudeUnits)
```
Sets the units of the magnitude axis of the plot or the returned `magnitude`
vector. See [Units](#units) for a list of valid units. Note that, depending on
the specified units, this option sets the estimation type as spectral density or
spectrum.

----

```
FFTLib.PlotSpectrum(__, loadImpedance)
[__] = FFTLib.GetSpectrum(__, loadImpedance)
```
Sets the load impedance (in Ohms) for the estimation of the power spectral
density or power spectrum.

----

```
FFTLib.PlotSpectrum(__, frequencyRange)
[__] = FFTLib.GetSpectrum(__, frequencyRange)
```
Sets the frequency range of the estimate. Valid options are `'centered'` for a
range of -Fs/2 to Fs/2, `'onesided'` for a range of 0 to Fs/2, or `'twosided'`
for a range of 0 to Fs, where Fs is equal to `samplingFrequency`. Note that a
`'onesided'` estimate is greater than a `'centered'` or `'twosided'` estimate by
a factor of 2 for power estimates or sqrt(2) for amplitude estimates.

----

```
FFTLib.PlotSpectrum(__, signalType)
[__] = FFTLib.GetSpectrum(__, signalType)
```
Specifies whether `signal` is a real passband signal or the complex envelope of
a real passband signal. Valid options are `'passband'` or `'envelope'`. In
either case, the spectrum estimate always represents the provided or
corresponding real passband signal; see [More About](#more-about) for more
information.

Note: while FFTLib does determine the signal type automatically by
checking if the signal has an imaginary part, this option may be useful for
passband signals that, due to some processing, may have a negligible leftover
imaginary part. Alternatively, the `'passband'` option may be used to disable
the scaling of a complex envelope `signal` and display or return its actual
spectrum estimate.

----

```
FFTLib.PlotSpectrum(__, windowFunction, windowArguments)
[__] = FFTLib.GetSpectrum(__, windowFunction, windowArguments)
```
Sets the name of the window function; all input arguments following
`windowFunction` are assumed to be the input arguments of the window function.
The first input argument of the window function, which is assumed to be the
length of the window, is provided by FFTLib by default.
Examples: `FFTLib.PlotSpectrum(__, 'hanning', 'periodic')`,
`FFTLib.PlotSpectrum(__, 'kaiser', 2.5)`.

FFTLib always attempts to use the optimum window overlap by maximizing the
difference between the amplitude flatness and the overlap correlation of the
window being used.

----

```
outStruct = FFTLib.PlotSpectrum(__)
[__, outStruct] = FFTLib.GetSpectrum(__)
```
Returns a struct containing a handle for the plotted lineseries (`PlotHandle`),
the frequency resolution of the spectrum (`FrequencyResolution`), the effective
noise bandwidth of the window (`ENBW`), and the normalized effective noise
bandwidth of the window (`NENBW`).




<br><br>




## Units
Frequency and magnitude units should be provided as character arrays containing
valid SI units. All SI prefixes (except hecto, deca, deci and centi) are
supported. The `'dB'` modifier may be used to convert magnitude units into
logarithmic units; note that `'dBW/Hz'` actually means dB(W/Hz). The `'log'`
modifier may be used to set the scaling of the corresponding plot axis to
logarithmic mode. The `'dBm'` unit may be used instead of `'dBmW'`.

The spectrum type is defined by the (base) magnitude units. For power spectral
density, use `'W/Hz'`. For amplitude spectral density, use `'V/rHz'` (volts per
the square root of hertz). For power spectrum, use `'W'`. For amplitude
spectrum, use `'V'`. For normalized power spectral density, use `'normalized'`.


<br>


#### List of valid units
Base Units: `'Hz'`, `'W'`, `'W/Hz'`, `'V'`, `'V/rHz'`<br>
SI Prefixes: `'Y'`, `'Z'`, `'E'`, `'P'`, `'T'`, `'G'`, `'M'`, `'k'`,
             `'m'`, `'u'`, `'n'`, `'p'`, `'f'`, `'a'`, `'z'`, `'y'`<br>
Modifiers: `'dB'`, `'log'`<br>
Other: `'dBm'`, `'dBm/Hz'`, `'normalized'`

Examples: `'MHz'`, `'dBW/Hz'`, `'mV'`, `'uV/rHz'`, `'loguV/rHz'`, `'normalized'`




<br><br>




## Examples
```
FFTLib.PlotSpectrum(signal, samplingFrequency, frequencyResolution, 'GHz', 'dBW/Hz');
FFTLib.PlotSpectrum(signal, samplingFrequency, frequencyResolution, 'MHz', 'dBm');
FFTLib.PlotSpectrum(signal, samplingFrequency, frequencyResolution, 'kHz', 'loguV/rHz', 'onesided');
[magnitude, frequency] = FFTLib.GetSpectrum(signal, samplingFrequency, frequencyResolution, 'MHz', 'W/Hz', 'hanning', 'periodic');
```

See `FFTLib_Test_Passband.m` and `FFTLib_Test_Envelope.m` for more examples.




<br><br>




## More About
Let $`\tilde{x}(t)`$ be the complex envelope of a real modulated signal $`x(t)`$
such that $`x(t) = \text{Re}\{\tilde{x}(t) \exp(j\omega_c t)\}`$.

It can be demonstrated that $`P_{av}[x(t)] = \frac{1}{2}P_{av}[\tilde{x}(t)]`$,
where $`P_{av}[\cdot]`$ denotes the average power. For this reason, and
in order to prevent unfortunate mistakes in the lab, FFTLib always plots or
returns the spectrum or spectral density estimate of $`x(t)`$:
even when `signal` is the complex envelope $`\tilde{x}(t)`$.

All spectra relate to average power or root mean square voltage.



<br><br>




## References
G. Heinzel, A. Rüdiger, and R. Schilling. (Feb. 2002). Spectrum and spectral
density estimation by the Discrete Fourier transform (DFT), including a
comprehensive list of window functions and some new flat-top windows.
Max Planck Society, [Online]. Available:
http://hdl.handle.net/11858/00-001M-0000-0013-557A-5